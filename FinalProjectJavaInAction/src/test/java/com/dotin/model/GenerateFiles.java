package com.dotin.model;

import com.dotin.logic.BasketIO;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;

public class GenerateFiles {
    int FILE_NUM = 10;

    @Ignore
    @Test
    public void generateFiles() throws JAXBException {
        for (int i = 0; i < FILE_NUM; i++) {
            String filePath = "src/main/resources/file" + ((i < 10) ? "0" : "") + i + ".xml";
            File file = new File(filePath);
            DomainGenerator domainGenerator = new DomainGenerator();
            Basket basket = domainGenerator.generateBasket();
            BasketIO.writeBasketToFile(basket, file);
        }
    }
}
