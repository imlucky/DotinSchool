package com.dotin.logic;

import com.dotin.model.AppleRange;
import org.junit.Test;

import java.io.File;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;

public class ApplicationTest {

    @Test
    public void testAnalyzingFilesInResourcesAsyncMode() {
        File directory = new File("src/main/resources");
        Map<String, Integer> resultMap = BasketAnalyze.analyzeBasketsAsynchronousInDirectoryColor(directory);
        System.out.println(resultMap);
        assertThat(resultMap, hasEntry("red", 32));
        assertThat(resultMap, hasEntry("green", 28));
        assertThat(resultMap, hasEntry("blue", 31));
        assertThat(resultMap, hasEntry("yellow", 39));
    }

    @Test
    public void testBasketAnalyzeWeight() {
        File directory = new File("src/main/resources");
        Map<AppleRange, Integer> resultMap = BasketAnalyze.analyzeBasketsAsynchronousInDirectoryWeight(directory);
        System.out.println(resultMap);
        assertThat(resultMap, hasEntry(AppleRange.LIGHTWEIGHT, 38));
        assertThat(resultMap, hasEntry(AppleRange.NORMAL, 45));
        assertThat(resultMap, hasEntry(AppleRange.HEAVY, 47));

    }
}

