package com.dotin.logic;

import com.dotin.model.Apple;
import com.dotin.model.AppleRange;
import com.dotin.model.Basket;
import com.dotin.model.DomainGenerator;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BasketIOTest {
    @Test
    public void testAppleMarshal() throws JAXBException {
        Apple apple = new Apple("red", AppleRange.HEAVY);
        JAXBContext jaxbContext = JAXBContext.newInstance(Apple.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(apple, new File("sampleApple.xml"));
    }

    @Test
    public void testUnMarshaller() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Apple.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Apple apple = (Apple) unmarshaller.unmarshal(new File("sampleApple.xml"));
        assertThat(apple.getColor(), equalTo("red"));
        assertThat(apple.getWeight(), equalTo(AppleRange.HEAVY));
    }

    @Test
    public void testBasketMarshal() throws JAXBException {
        DomainGenerator basketGenerator = new DomainGenerator();
        Basket basket = basketGenerator.generateBasket();
        BasketIO.writeBasketToFile(basket, new File("sampleBasket.xml"));
    }

    @Test
    public void testBasketUnMarshaller() throws JAXBException {
        Basket basket = BasketIO.readBasketFromFile(new File("sampleBasket.xml"));
        assertThat(basket.getApples().size(), equalTo(10));
    }


}
