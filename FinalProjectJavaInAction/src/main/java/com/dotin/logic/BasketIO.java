package com.dotin.logic;

import com.dotin.model.Basket;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class BasketIO {
    public static void writeBasketToFile(Basket basket, File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Basket.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(basket, file);
    }

    public static Basket readBasketFromFile(File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Basket.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Basket baskets = (Basket) unmarshaller.unmarshal(file);
        return baskets;
    }
}
