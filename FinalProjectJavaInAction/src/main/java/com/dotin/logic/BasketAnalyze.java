package com.dotin.logic;

import com.dotin.model.Apple;
import com.dotin.model.AppleRange;
import com.dotin.model.Basket;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

public class BasketAnalyze {

    public static Map<String, Integer> analyzeBasketColor(Basket basket) {
        Map<String, Long> collect = basket.getApples().stream().collect(groupingBy(Apple::getColor, counting()));
        return collect.keySet().stream().collect(toMap(Function.identity(), key -> (collect.get(key)).intValue()));
    }

    public static Map<String, Integer> analyzeBasketsAsynchronousInDirectoryColor(File directory) {
        List<CompletableFuture<Map<String, Integer>>> statisticMapList = Arrays.stream(directory.listFiles())
                .map(BasketAnalyze::xmlFileToCompletableFutureBasketFunction)
                .map(fut -> fut.thenCompose(
                        basket -> CompletableFuture.supplyAsync(() -> BasketAnalyze.analyzeBasketColor(basket))
                )).collect(toList());

        List<Map<String, Integer>> completedStatistics = statisticMapList.stream()
                .map(CompletableFuture::join).collect(toList());
        Map<String, Integer> resultMap = new HashMap<>();
        for (Map<String, Integer> map : completedStatistics) {
            map.forEach((color, num) -> resultMap.merge(color, num, Integer::sum));
        }
        return resultMap;
    }

    public static Map<AppleRange, Integer> analyzeBasketWeight(Basket basket) {
        Map<AppleRange, Long> collect = basket.getApples().stream().collect(groupingBy(Apple::getWeight, counting()));
        return collect.keySet().stream().collect(toMap(Function.identity(), key -> (collect.get(key)).intValue()));
    }

    public static Map<AppleRange, Integer> analyzeBasketsAsynchronousInDirectoryWeight(File directory) {
        List<CompletableFuture<Map<AppleRange, Integer>>> statisticMapList = Arrays.stream(directory.listFiles())
                .map(BasketAnalyze::xmlFileToCompletableFutureBasketFunction)
                .map(fut -> fut.thenCompose(
                        basket -> CompletableFuture.supplyAsync(() -> BasketAnalyze.analyzeBasketWeight(basket))
                )).collect(toList());

        List<Map<AppleRange, Integer>> completedStatistics = statisticMapList.stream()
                .map(CompletableFuture::join).collect(toList());
        Map<AppleRange, Integer> resultMap = new HashMap<>();
        for (Map<AppleRange, Integer> map : completedStatistics) {
            map.forEach((weight, num) -> resultMap.merge(weight, num, Integer::sum));
        }
        return resultMap;
    }

    private static CompletableFuture<Basket> xmlFileToCompletableFutureBasketFunction(File file) {
        CompletableFuture<Basket> basketFuture = new CompletableFuture<>();
        new Thread(() -> {
            try {
                Basket basket = BasketIO.readBasketFromFile(file);
                basketFuture.complete(basket);
            } catch (JAXBException e) {
                basketFuture.completeExceptionally(e);
            }
        }).start();
        return basketFuture;
    }
}
