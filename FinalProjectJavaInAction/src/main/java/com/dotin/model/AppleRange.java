package com.dotin.model;

public enum  AppleRange {
    LIGHTWEIGHT, NORMAL, HEAVY
}
