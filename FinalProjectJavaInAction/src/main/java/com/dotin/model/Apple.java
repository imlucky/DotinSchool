package com.dotin.model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlType(propOrder = {"color", "weight"})
public class Apple {
    private String color;
    @XmlJavaTypeAdapter(EnumAdaptor.class)
    private AppleRange weight;

    public Apple() {
    }

    public Apple(String color, AppleRange weight) {
        this.weight = weight;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public AppleRange getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "color='" + color + '\'' +
                ", weight=" + weight +
                '}';
    }
}
