package com.dotin.model;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlRootElement(name = "Baskets")
public class Basket {
    @XmlElementWrapper(name = "Basket")
    @XmlElement(name = "apple")
    private List<Apple> appleList;

    public Basket() {
        appleList = new ArrayList<>();
    }

    public Basket(List<Apple> appleList) {
        setApples(appleList);
    }

    public List<Apple> getApples() {
        return appleList;
    }

    public void setApples(List<Apple> appleList) {
        if (appleList == null) {
            throw new NullPointerException("list can not be NULL");
        }
        this.appleList = appleList;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "appleList=" + appleList +
                '}';
    }
}
