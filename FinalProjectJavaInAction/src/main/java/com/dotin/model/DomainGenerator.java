package com.dotin.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DomainGenerator {
    private final List<String> colorList = new ArrayList<>(Arrays.asList("red", "yellow", "green", "blue"));
    private final List<AppleRange> weightList = new ArrayList<>(Arrays.asList(AppleRange.LIGHTWEIGHT, AppleRange.NORMAL, AppleRange.HEAVY));
    Random random = new Random();

    public Apple generateApple() {
        Apple apple = new Apple(randomColor(), randomWeight());
        return apple;
    }

    public Basket generateBasket() {
        List<Apple> appleList = new ArrayList<>();
        for (int i = 0; i < random.nextInt(10) + 10; i++) {
            appleList.add(generateApple());
        }
        return new Basket(appleList);
    }

    private AppleRange randomWeight() {
        int weight = random.nextInt(3);
        return weightList.get(weight);
    }

    private String randomColor() {
        int colorIndex = random.nextInt(4);
        return colorList.get(colorIndex);
    }
}
