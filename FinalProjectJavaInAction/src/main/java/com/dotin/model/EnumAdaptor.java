package com.dotin.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class EnumAdaptor extends XmlAdapter<String, AppleRange> {

    public AppleRange unmarshal(String value) {
        return AppleRange.valueOf(value);
    }

    public String marshal(AppleRange value) {
        return value.toString();
    }
}
