project list:
=============
* Java8InAction: Examples in Chapter 2,3
* Stream: Examples in chapter 4,5 and 6
* Optionals: Example in chapter 10
* CompletableFuture: Example chapter11
* FinalProjectJavaInAction: finally project from java8InAction Book
* SpringTest: Examples in chapter 5
* Hibernate: Strategies(SingelTable, TablePerClass, Joined)