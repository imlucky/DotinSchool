package com.dotin.chapter3.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BeanInstantiationCondition implements Condition {
    private Boolean instantiateBean;

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        Properties properties = new Properties();
        try {
            InputStream inputStream = new FileInputStream(new File("src/main/resources/instantiate-bean.properties"));
            properties.load(inputStream);
            instantiateBean = Boolean.valueOf(properties.getProperty( "instantiate"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return instantiateBean;
        // return true;
    }
}
