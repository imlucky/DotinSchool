package com.dotin.chapter3.condition;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    private static ApplicationContext context;
    static{
        context = new AnnotationConfigApplicationContext(ConditionalBeanConfig.class);
    }


    public static void main(String[] args) {
        IntValueBean bean = context.getBean(IntValueBean.class);
        System.out.println(bean);
    }
}
