package com.dotin.chapter3.primarybeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Menu {

    @Autowired
    private Dessert dessert;

    public Dessert getDessert() {
        return dessert;
    }
}
