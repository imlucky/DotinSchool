package com.dotin.chapter3.primarybeans;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.dotin.chapter3.primarybeans"})
public class MenuAppConfig {

}
