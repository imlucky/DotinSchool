package com.dotin.chapter3.profilebean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    private static ApplicationContext context = new AnnotationConfigApplicationContext(ProfileConfigBean.class);


    public static void main(String[] args) {
        ValuedBean valuedBean = context.getBean(ValuedBean.class);
        System.out.println(valuedBean.getValue());

    }
}
