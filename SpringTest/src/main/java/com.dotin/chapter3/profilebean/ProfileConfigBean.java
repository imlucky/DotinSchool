package com.dotin.chapter3.profilebean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ProfileConfigBean {
    @Bean
    @Profile("develop")
    public ValuedBean getBeanWhenProfile1IsActive(){
        return new ValuedBean("Profile in develop environment");
    }

    @Bean
    @Profile("test")
    public ValuedBean getBeanWhenProfile2IsActive(){
        return new ValuedBean("Profile in test environment");
    }
}
