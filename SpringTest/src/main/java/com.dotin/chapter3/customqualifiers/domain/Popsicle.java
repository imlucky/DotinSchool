package com.dotin.chapter3.customqualifiers.domain;


import com.dotin.chapter3.customqualifiers.qualifiers.Cold;
import com.dotin.chapter3.customqualifiers.qualifiers.Fruity;
import org.springframework.stereotype.Component;

@Component
@Fruity
@Cold
public class Popsicle implements Dessert {
}
