package com.dotin.chapter3.customqualifiers.domain;


import com.dotin.chapter3.customqualifiers.qualifiers.Cold;
import com.dotin.chapter3.customqualifiers.qualifiers.Creamy;
import org.springframework.stereotype.Component;

@Component
@Creamy
@Cold
public class IceCream implements Dessert {
}
