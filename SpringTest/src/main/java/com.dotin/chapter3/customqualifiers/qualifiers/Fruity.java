package com.dotin.chapter3.customqualifiers.qualifiers;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target(value = {CONSTRUCTOR, FIELD, TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Fruity {
}
