package com.dotin.chapter3.customqualifiers;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.dotin.chapter3.customqualifiers.domain"})
public class CustomQualifiersConfig {
}
