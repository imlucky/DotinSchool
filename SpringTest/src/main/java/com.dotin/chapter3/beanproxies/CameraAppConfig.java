package com.dotin.chapter3.beanproxies;


import com.dotin.chapter3.beanproxies.domain.Camera;
import com.dotin.chapter3.beanproxies.domain.FlashNegative;
import com.dotin.chapter3.beanproxies.domain.Negative;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class CameraAppConfig {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Negative getNegative() {
        return new FlashNegative();
    }

    @Bean
    public Camera getCamera() {
        return new Camera();
    }
}
