package com.dotin.chapter3.beanproxies.domain;

public class FlashNegative implements Negative {

    private static long serialNumberCounter = 100;
    private long serialNumber;

    public FlashNegative() {
        serialNumber = serialNumberCounter;
        serialNumberCounter++;

    }

    @Override
    public long getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String toString() {
        return "FlashNegative{" +
                "serialNumber=" + serialNumber +
                '}';
    }

}
