package com.dotin.chapter3.beanproxies.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class Camera {

    @Autowired
    private ApplicationContext context;


    public Camera() {
    }

    /**
     * every photo should be taken on new instance of negative and should have different serial number;
     */
    public void takeAPhotoOf(String photoContent) {
        Negative rawNegative = context.getBean(Negative.class);
        System.out.println("photo of " + photoContent + "is taken on with serialNumber: " + rawNegative.getSerialNumber());
    }
}
