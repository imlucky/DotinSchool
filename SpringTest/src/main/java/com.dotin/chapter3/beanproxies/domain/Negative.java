package com.dotin.chapter3.beanproxies.domain;

public interface Negative {

    long getSerialNumber();
}
