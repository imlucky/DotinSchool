package com.dotin.chapter3.injectingExternalValues.usingEnvironment;


import com.dotin.chapter3.injectingExternalValues.domain.CustomDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource({"classpath:injectingExternalValues/album.properties"})
public class UsingEnvironmentConfig {
    @Autowired
    Environment environment;

    @Bean
    public CustomDisc getAlbumWithExternalValues() {
        return new CustomDisc(environment.getRequiredProperty("album.title"),
                environment.getRequiredProperty("album.artist"));
    }
}
