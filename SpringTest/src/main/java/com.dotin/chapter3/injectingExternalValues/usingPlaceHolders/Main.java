package com.dotin.chapter3.injectingExternalValues.usingPlaceHolders;

import com.dotin.chapter3.injectingExternalValues.domain.CustomDisc;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(UsingPlaceHoldersConfig.class);
        CustomDisc customDisc = context.getBean(CustomDisc.class);
        System.out.println(customDisc.getArtist() + customDisc.getTitle());
    }
}
