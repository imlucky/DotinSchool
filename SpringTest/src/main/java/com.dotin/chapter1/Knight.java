package com.dotin.chapter1;

public interface Knight {
    void embarkOnQuest();
}
