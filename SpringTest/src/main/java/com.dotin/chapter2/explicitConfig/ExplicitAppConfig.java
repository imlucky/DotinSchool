package com.dotin.chapter2.explicitConfig;

import com.dotin.chapter2.maindomainobjects.CDPlayer;
import com.dotin.chapter2.maindomainobjects.CompactDisc;
import com.dotin.chapter2.maindomainobjects.MediaPlayer;
import com.dotin.chapter2.maindomainobjects.SgtPeppers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExplicitAppConfig {

    @Bean
    public MediaPlayer getMediaPlayer(CompactDisc cd) {
        return new CDPlayer(cd);
    }

    @Bean
    public CompactDisc getCompactDisk() {
        return new SgtPeppers();
    }
}
