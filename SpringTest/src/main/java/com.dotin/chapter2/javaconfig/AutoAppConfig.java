package com.dotin.chapter2.javaconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.dotin.chapter2.maindomainobjects"})
public class AutoAppConfig {
}
