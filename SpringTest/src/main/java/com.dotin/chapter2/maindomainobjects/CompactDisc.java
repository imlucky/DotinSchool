package com.dotin.chapter2.maindomainobjects;

public interface CompactDisc {
    void play();
}
