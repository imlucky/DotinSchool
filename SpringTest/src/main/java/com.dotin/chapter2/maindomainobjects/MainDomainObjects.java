package com.dotin.chapter2.maindomainobjects;

import com.dotin.chapter2.explicitConfig.ExplicitAppConfig;
import com.dotin.chapter2.javaconfig.AutoAppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainDomainObjects {
    public static void main(String[] args) {
        //autoWiringActivateComponentScanViaXML();
        //autoWiringActivateComponentScanViaJava();
        //explicitJavaConfig();
        //explicitXmlConfig();
        //mixJavaConfigToJavaConfig();
        //mixXMLConfigToJavaConfig();
        //mixJavaConfigToXMLConfig();
        mixXMLConfigToXMLConfig();
    }

    public static void autoWiringActivateComponentScanViaXML() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/autoWiringActivateComponentScanViaXml");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void autoWiringActivateComponentScanViaJava() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AutoAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void explicitJavaConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ExplicitAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void explicitXmlConfig() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/mediaPlayerConfig");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void mixJavaConfigToJavaConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(com.dotin.chapter2.mix.javaConfig.toJavaConfig.MixingJavaToJavaAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void mixXMLConfigToJavaConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(com.dotin.chapter2.mix.xmlConfig.toJavaConfig.MixingXMLToJavaAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void mixJavaConfigToXMLConfig() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/importingJavaConfig");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    public static void mixXMLConfigToXMLConfig() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/importingXMLConfig");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        //MediaPlayer mediaPlayer = context.getBean("mediaPlayer", MediaPlayer.class);
        mediaPlayer.playDisk();
    }

}
