package com.dotin.chapter2.maindomainobjects;

import org.springframework.stereotype.Component;

@Component
public class CDPlayer implements MediaPlayer {
    private CompactDisc cd;
    //private CompactDisc cd2;

    public CDPlayer(CompactDisc cd) {
        this.cd = cd;
    }

    /*public CDPlayer(CompactDisc cd, CompactDisc cd2) {
        this.cd = cd;
        this.cd2 = cd2;
    }*/

    @Override
    public void playDisk() {
        //System.out.println(cd == cd2);
        cd.play();
    }
}
