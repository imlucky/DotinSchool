package com.dotin.chapter2.mix.javaConfig.toJavaConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(com.dotin.chapter2.explicitConfig.ExplicitAppConfig.class)
public class MixingJavaToJavaAppConfig {
}
