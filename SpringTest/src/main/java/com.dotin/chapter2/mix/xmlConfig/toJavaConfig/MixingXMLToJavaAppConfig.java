package com.dotin.chapter2.mix.xmlConfig.toJavaConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {"spring/mediaPlayerConfig"})
public class MixingXMLToJavaAppConfig {
}
