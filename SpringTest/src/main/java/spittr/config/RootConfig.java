package spittr.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages = {"spittr"}, excludeFilters = @Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class))
//@ComponentScan(basePackages = {"spitter"}, excludeFilters = @Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)) page 138
public class RootConfig {
}
