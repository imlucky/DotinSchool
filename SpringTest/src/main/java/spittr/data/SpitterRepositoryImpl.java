package spittr.data;

import org.springframework.stereotype.Repository;
import spittr.Spitter;

@Repository
public class SpitterRepositoryImpl implements SpitterRepository {
    @Override
    public Spitter save(Spitter spitter) {
        return spitter;
    }

    @Override
    public Spitter findByUsername(String username) {
        return new Spitter(24L, username, "24hours", "Jack", "Bauer");
    }
}
