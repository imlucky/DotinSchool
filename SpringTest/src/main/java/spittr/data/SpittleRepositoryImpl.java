package spittr.data;

import org.springframework.stereotype.Repository;
import spittr.Spittle;

import java.util.*;

@Repository
public class SpittleRepositoryImpl implements SpittleRepository {
    @Override
    public List<Spittle> findRecentSpittles() {
        List<Spittle> spittles = new ArrayList<>();
        spittles.add(new Spittle("java", new Date()));
        spittles.add(new Spittle("javaScript", new Date()));;
        return spittles;
    }


    @Override
    public List<Spittle> findSpittles(long max, int count) {
        List<Spittle> spittles = new ArrayList<>();
        spittles.add(new Spittle("salam", new Date()));
        spittles.add(new Spittle("ahmad", new Date()));
        spittles.add(new Spittle("halet", new Date()));
        spittles.add(new Spittle("khobe?", new Date()));

        return spittles;
    }

    @Override
    public Spittle findOne(long id) {
        return new Spittle("holiday", new Date());
    }

    @Override
    public void save(Spittle spittle) {

    }
}
