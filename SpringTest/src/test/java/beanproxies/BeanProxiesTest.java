package beanproxies;

import com.dotin.chapter3.beanproxies.CameraAppConfig;
import com.dotin.chapter3.beanproxies.domain.Camera;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CameraAppConfig.class)
public class BeanProxiesTest {

    @Autowired
    private ApplicationContext context;

    @Test
    public void testCameraInjection() {
        Camera camera = context.getBean(Camera.class);
        assertThat(camera, is(notNullValue()));
    }

    @Test
    public void testNegativeInstantiation() {
        Camera camera = context.getBean(Camera.class);
        camera.takeAPhotoOf("Hummer ");
        camera.takeAPhotoOf("Audi ");
        camera.takeAPhotoOf("Chevrolet ");
    }
}
