package customqualifiers;


import com.dotin.chapter3.customqualifiers.CustomQualifiersConfig;
import com.dotin.chapter3.customqualifiers.domain.Dessert;
import com.dotin.chapter3.customqualifiers.domain.IceCream;
import com.dotin.chapter3.customqualifiers.domain.Popsicle;
import com.dotin.chapter3.customqualifiers.qualifiers.Cold;
import com.dotin.chapter3.customqualifiers.qualifiers.Creamy;
import com.dotin.chapter3.customqualifiers.qualifiers.Fruity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomQualifiersConfig.class)
public class CustomQualifiersTest {


    @Autowired
    @Cold
    @Fruity
    private Dessert popsicle;

    @Autowired
    @Cold
    @Creamy
    private Dessert iceCream;

    @Test
    public void popsicleIsWiredProperly() {
        assertThat(popsicle, is(instanceOf(Popsicle.class)));
    }

    @Test
    public void iceCreamIsWiredProperly() {
        assertThat(iceCream, is(instanceOf(IceCream.class)));
    }
}
