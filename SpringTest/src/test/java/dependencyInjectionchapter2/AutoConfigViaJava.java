package dependencyInjectionchapter2;

import com.dotin.chapter2.javaconfig.AutoAppConfig;
import com.dotin.chapter2.maindomainobjects.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AutoAppConfig.class)
public class AutoConfigViaJava {

    @Autowired
    private CompactDisc cd;

    @Test
    public void cdIsNotNullAfterAutoWiringViaJava() {
        assertThat(cd, is(notNullValue()));
    }
}

