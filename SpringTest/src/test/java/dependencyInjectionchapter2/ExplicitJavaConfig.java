package dependencyInjectionchapter2;

import com.dotin.chapter2.explicitConfig.ExplicitAppConfig;
import com.dotin.chapter2.maindomainobjects.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ExplicitAppConfig.class)
public class ExplicitJavaConfig {

    @Autowired
    private CompactDisc cd;

    @Test
    public void cdIsNotNullAfterWiringViaJavaConfig() {
        assertThat(cd, is(notNullValue()));
    }
}
