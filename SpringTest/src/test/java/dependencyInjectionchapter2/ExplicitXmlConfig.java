package dependencyInjectionchapter2;

import com.dotin.chapter2.maindomainobjects.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mediaPlayerConfig")
public class ExplicitXmlConfig {

    @Autowired
    private CompactDisc cd;
    @Autowired
    private ApplicationContext context;

    @Test
    public void testCompactDiskIsNotNullViaXMLConfiguration() {
        assertThat(cd, is(notNullValue()));
    }

    @Test
    public void testCompactDiscRetrievalViaApplicationContext() {
        CompactDisc cd = context.getBean(CompactDisc.class);
        assertThat(cd, is(notNullValue()));
    }

}
