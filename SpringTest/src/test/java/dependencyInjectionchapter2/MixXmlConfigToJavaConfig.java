package dependencyInjectionchapter2;

import com.dotin.chapter2.maindomainobjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = com.dotin.chapter2.mix.xmlConfig.toJavaConfig.MixingXMLToJavaAppConfig.class)
public class MixXmlConfigToJavaConfig {
    @Autowired
    private MediaPlayer mp;

    @Test
    public void testMediaPlayerIsNotNullWhileMixingXmlConfigToJavaConfig() {
        assertThat(mp, is(notNullValue()));
        mp.playDisk();
    }
}
