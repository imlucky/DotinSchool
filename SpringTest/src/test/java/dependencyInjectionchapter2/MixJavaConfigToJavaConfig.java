package dependencyInjectionchapter2;

import com.dotin.chapter2.maindomainobjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = com.dotin.chapter2.mix.javaConfig.toJavaConfig.MixingJavaToJavaAppConfig.class)
public class MixJavaConfigToJavaConfig {

    @Autowired
    private ApplicationContext context;

    @Test
    public void mediaPlayerIsNotNullWhileMixingJavaConfigToJavaConfig() {
        MediaPlayer cd = context.getBean(MediaPlayer.class);
        assertThat(cd, is(notNullValue()));
    }
}
