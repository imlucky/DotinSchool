package profilebean;

import com.dotin.chapter3.profilebean.ProfileConfigBean;
import com.dotin.chapter3.profilebean.ValuedBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ProfileConfigBean.class)
@ActiveProfiles("develop")
public class ProfileBeanTest {
    @Autowired
    private ValuedBean valuedBean;

    @Test
    public void checkProfile1ProperBeanIsCreated() {
        assertThat("develop is active", valuedBean.getValue(), is(equalTo("Profile in develop environment")));
    }
}
