package qualifier;

import com.dotin.chapter3.primarybeans.Dessert;
import com.dotin.chapter3.primarybeans.IceCream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = com.dotin.chapter3.primarybeans.MenuAppConfig.class)
public class UseBeanIdAsQualifier {
    @Autowired
    @Qualifier("iceCream")
    private Dessert dessert;

    @Test
    public void testUsingQualifierWithBeanId() {
        assertThat(dessert, is(instanceOf(IceCream.class)));
    }
}
