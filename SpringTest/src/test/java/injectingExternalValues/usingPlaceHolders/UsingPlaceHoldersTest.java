package injectingExternalValues.usingPlaceHolders;


import com.dotin.chapter3.injectingExternalValues.domain.CustomDisc;
import com.dotin.chapter3.injectingExternalValues.usingPlaceHolders.UsingPlaceHoldersConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UsingPlaceHoldersConfig.class)
public class UsingPlaceHoldersTest {
    @Autowired
    private CustomDisc disk;

    @Test
    public void testDiskInstantiation() {
        assertThat(disk, is(notNullValue()));
        assertThat(disk.getTitle(), is(equalTo("ya tab tab")));
        assertThat(disk.getArtist(), is(equalTo("nancy ajram")));
    }
}