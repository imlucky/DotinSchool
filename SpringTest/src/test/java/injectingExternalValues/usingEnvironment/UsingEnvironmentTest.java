package injectingExternalValues.usingEnvironment;


import com.dotin.chapter3.injectingExternalValues.domain.CustomDisc;
import com.dotin.chapter3.injectingExternalValues.usingEnvironment.UsingEnvironmentConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UsingEnvironmentConfig.class)
public class UsingEnvironmentTest {

    @Autowired
    private CustomDisc disc;

    @Test
    public void testDiskInstantiation() {
        assertThat(disc, is(notNullValue()));
        assertThat(disc.getTitle(), is(equalTo("ya tab tab")));
        assertThat(disc.getArtist(), is(equalTo("nancy ajram")));
    }
}