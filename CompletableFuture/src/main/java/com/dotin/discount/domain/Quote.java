package com.dotin.discount.domain;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Quote {
    private final String shopName;
    private final double price;
    private final DiscountCode code;

    private Quote(String shopName, double price, DiscountCode code) {
        this.shopName = shopName;
        this.price = price;
        this.code = code;
    }

    public static Quote parse(String string) {
        String[] array = string.split(":");
        // Arrays.stream(array).forEach(System.out::println);
        String shopName = array[0];
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        double price;
        try {
            price = nf.parse(array[1]).doubleValue();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        DiscountCode discountCode = DiscountCode.valueOf(array[2]);
        return new Quote(shopName, price, discountCode);
    }

    public String getShopName() {
        return shopName;
    }

    public double getPrice() {
        return price;
    }

    public DiscountCode getCode() {
        return code;
    }
}
