package com.dotin.discount.service;

import com.dotin.discount.domain.DiscountCode;
import com.dotin.discount.domain.Quote;
import com.dotin.util.DelayUtil;

public class DiscountUtilizeService {
    public static String applyDiscount(Quote quote) {
        DelayUtil.delayOSec();
        return quote.getShopName() + " price is " + applyDiscountCode(quote.getPrice(), quote.getCode());
    }

    private static double applyDiscountCode(double price, DiscountCode code) {
        double discountValue = price * code.getPercentage() / 100;
        return price - discountValue;
    }
}
