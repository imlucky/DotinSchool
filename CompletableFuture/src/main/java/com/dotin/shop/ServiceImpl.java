package com.dotin.shop;

import com.dotin.util.VendorList;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static java.util.stream.Collectors.toList;

public class ServiceImpl implements Service {

    public List<String> findPrices(String product, int shopName) {
        return VendorList.getShops(shopName).stream().map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice(product)))
                .collect(toList());
    }

    public List<String> findPricesParallel(String product, int shopName) {
        return VendorList.getShops(shopName).parallelStream().map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice(product)))
                .collect(toList());
    }

    private final Executor executor = Executors.newFixedThreadPool(Math.min(VendorList.getShops(5).size(), 100), new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        }
    });

    public List<CompletableFuture<String>> findPricesCompletableFuture(String product, int shopName) {
        return VendorList.getShops(shopName).stream().map(shop -> CompletableFuture.supplyAsync(() ->
                String.format("%s price is %.2f", shop.getName(), shop.getPrice(product), executor)))
                .collect(toList());
    }

    //join
    public List<String> findPricesCompletableFutureJoin(String product, int shopName) {
        List<CompletableFuture<String>> priceFuture = VendorList.getShops(shopName).stream().map(shop -> CompletableFuture.supplyAsync(() ->
                shop.getName() + "price is " + shop.getPrice(product)))
                .collect(toList());
        return priceFuture.stream().map(CompletableFuture::join).collect(toList());
    }

   /* @Override
    public List<String> findPricesDiscount(String product) {
        return VendorList.getShops().stream()
                .map(shop -> shop.getPrice(product))
                .map(Quote::parse)
                .map(DiscountService::applyDiscount)
                .collect(toList());
    }*/
}
