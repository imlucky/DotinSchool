package com.dotin.shop.service;

import com.dotin.discount.domain.Quote;
import com.dotin.discount.service.DiscountUtilizeService;
import com.dotin.util.ExecutorUtil;
import com.dotin.util.VendorList;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static java.util.stream.Collectors.toList;


    public class DiscountedShopService {
        public static List<String> findPricesSynchronous(String product, int shopNum) {
            List<String> resultList = VendorList.getShops(shopNum).stream()
                    .map(shop -> shop.getPriceAndDiscount(product)).map(Quote::parse)
                    .map(DiscountUtilizeService::applyDiscount).collect(toList());
            return resultList;
        }

        public static List<String> findPricesAsynchronousOnCommonPool(String product , int shopNum) {
            List<CompletableFuture<String>> resultFutureList = VendorList.getShops(shopNum).stream()
                    .map(shop -> CompletableFuture.supplyAsync(() -> shop.getPriceAndDiscount(product)))
                    .map(future -> future.thenApply(Quote::parse))
                    .map(future -> future.thenCompose(quote -> CompletableFuture.supplyAsync(() -> DiscountUtilizeService.applyDiscount(quote))))
                    .collect(toList());
            return resultFutureList.stream().map(CompletableFuture::join).collect(toList());
        }

        public static List<String> findPricesAsynchronousOnCustomExecutor(String product, int shopNum) {
            int threadNum = Math.min(shopNum, 100);
            Executor executor = ExecutorUtil.getExecutor(threadNum);

            List<CompletableFuture<String>> resultFutureList = VendorList.getShops(shopNum).stream()
                    .map(shop -> CompletableFuture.supplyAsync(() -> shop.getPriceAndDiscount(product), executor))
                    .map(future -> future.thenApply(Quote::parse))
                    .map(future -> future.thenCompose(quote -> CompletableFuture.supplyAsync(() -> DiscountUtilizeService.applyDiscount(quote), executor)))
                    .collect(toList());
            return resultFutureList.stream().map(CompletableFuture::join).collect(toList());
        }
    }

