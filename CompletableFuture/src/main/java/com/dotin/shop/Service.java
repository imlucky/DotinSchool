package com.dotin.shop;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface Service {
    List<String> findPrices(String product, int shopName);
    List<String> findPricesParallel(String product, int shopName);
    List<CompletableFuture<String>> findPricesCompletableFuture(String product, int shopName);
    List<String> findPricesCompletableFutureJoin(String product, int shopName);
}
