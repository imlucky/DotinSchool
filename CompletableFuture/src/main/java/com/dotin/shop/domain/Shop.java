package com.dotin.shop.domain;

import com.dotin.discount.domain.DiscountCode;
import com.dotin.util.DelayUtil;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class Shop {
    private static Random random = new Random();
    private String name;

    public Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getPrice(String product) {
        return calculatePrice(product);
    }

    public String getPriceAndDiscount(String product) {
        DiscountCode[] discountsArray = DiscountCode.values();

        String result = "";
        result += getName() + ":";
        result += String.valueOf(calculatePrice(product)) + ":";
        result += discountsArray[new Random().nextInt(discountsArray.length)];
        return result;
    }

    private double calculatePrice(String product) {
        DelayUtil.delayOSec();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }


    public CompletableFuture<Double> getPriceAsync(String product) {
        CompletableFuture<Double> priceF = new CompletableFuture<>();
        Thread thread = new Thread(() -> {
            double price = calculatePrice(product);
            try {
                priceF.complete(price);
            } catch (Exception ex) {
                priceF.completeExceptionally(ex);
            }
        });
        thread.start();
        return priceF;
    }

}
