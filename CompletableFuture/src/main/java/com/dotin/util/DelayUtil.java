package com.dotin.util;

import java.util.Random;

public class DelayUtil {
    public static void delayOSec() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void delayRandom1to3Sec() {
        Random random = new Random();
        long delay = random.nextInt(2000) + 1000L;
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
