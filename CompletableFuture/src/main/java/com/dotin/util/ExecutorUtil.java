package com.dotin.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class ExecutorUtil {
    public static Executor getExecutor(int threadsNum) {
        Executor executor = Executors.newFixedThreadPool(threadsNum, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });

        return executor;
    }
}

