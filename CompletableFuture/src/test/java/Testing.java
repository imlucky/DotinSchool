import org.junit.Test;

import java.util.Random;
import java.util.concurrent.*;

import static junit.framework.TestCase.*;

public class Testing {
    @Test
    public void completableFuture() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message");
        assertTrue(cf.isDone());
        assertEquals("message", cf.getNow(null));
    }

    @Test
    public void runAsync() {
        CompletableFuture<Void> cf = CompletableFuture.runAsync(
                () -> {
                    assertTrue(Thread.currentThread().isDaemon());
                    //randomSleep();
                });
        assertFalse(cf.isDone());
        //sleepEnough();
        //assertTrue(cf.isDone());
    }

    @Test
    public void thenApply() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApply(s -> {
            assertFalse(Thread.currentThread().isDaemon());
            return s.toUpperCase();
        });
        assertEquals("MESSAGE", cf.getNow(null));
    }

    Random random = new Random();
    static ExecutorService executor = Executors.newFixedThreadPool(3, new ThreadFactory() {
        int count = 1;

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "custom-executor" + count++);
        }
    });

    @Test
    public void thenApplyAsync() {
        CompletableFuture<String> cf = CompletableFuture.completedFuture("message").thenApplyAsync(s -> {
            assertTrue(Thread.currentThread().getName().startsWith("custom-executor"));
            assertFalse(Thread.currentThread().isDaemon());
            randomSleep();
            return s.toUpperCase();
        }, executor);
        assertNull(cf.getNow(null));
        assertEquals("MESSAGE", cf.join());
    }

    private Random randomSleeps() {
        Random random = new Random();
        return random;
    }

    @Test
    public void thenAccept() {
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture("thenAccept message").thenAccept(s -> result.append(s));
        assertTrue("result was empty", result.length() > 0);
        System.out.println(result);
    }

    @Test
    public void runAfterBoth() {
        String original = "message";
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture(original).thenApply(String::toUpperCase).runAfterBoth(
                CompletableFuture.completedFuture(original).thenApply(String::toLowerCase), () -> result.append("done")
        );
        assertTrue("result was empty", result.length() > 0);
    }

    @Test
    public void biConsumer() {
        String original = "javaBook";
        StringBuilder result = new StringBuilder();
        CompletableFuture.completedFuture(original).thenApply(String::toUpperCase).thenAcceptBoth(CompletableFuture.completedFuture(original)
                .thenApply(String::toLowerCase), (s1, s2) -> result.append(s1 + s2));
        assertEquals("JAVABOOKjavabook", result.toString());
    }

    public String delayedUpperCase(String s) {
        randomSleep();
        return s.toUpperCase();
    }

    public String DelayedLowerCase(String s) {
        randomSleep();
        return s.toLowerCase();
    }

    public void randomSleep() {
        try {
            Thread.sleep(random.nextInt(1000));
        } catch (InterruptedException e) {

        }
    }


    public void sleepEnough() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {

        }
    }


    @Test
    public void testThenCombine() {
        System.out.println("Retrieving weight");
        CompletableFuture<Double> weightInKgFuture = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException();
            }
            return 65.65;
        });

        System.out.println("Retrieving height");
        CompletableFuture<Double> heightInCmFuture = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException();
            }
            return 170.5;
        });

        System.out.println("Calculate BMI");
        CompletableFuture<Double> combineFuture = weightInKgFuture.thenCombine(heightInCmFuture, (weightInKg, heightInCm) -> {
            Double heightInMeter = heightInCm / 100;
            return weightInKg / (heightInMeter * heightInMeter);
        });
        try {
            System.out.println("your BMI is: " + combineFuture.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCompletableFuture() {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of Future 1";
        });

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(4);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of Future 2";
        });

        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of Future 3";
        });

        CompletableFuture<Object> anyOfFuture = CompletableFuture.anyOf(future1, future2, future3);

        try {
            System.out.println(anyOfFuture.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void exceptionHandeling() throws ExecutionException, InterruptedException {
        Integer age = 50;
        CompletableFuture<String> result = CompletableFuture.supplyAsync(() -> {

            if (age < 0) {
                throw new IllegalArgumentException("age is false");
            }
            if (age > 18) {
                return " Adult";
            } else {
                return " child";
            }
        }).exceptionally(ex -> {
            System.out.println("oops exception is " + ex.getMessage());
            return " unKnow";
        });

        System.out.println("maturity" + result.get());
    }


}
