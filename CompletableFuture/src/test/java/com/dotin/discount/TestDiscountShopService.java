package com.dotin.discount;

import com.dotin.shop.service.DiscountedShopService;
import org.junit.Test;

import java.util.List;

public class TestDiscountShopService {
    private static final int SHOP_NUM = 4;

    @Test
    public void testFindDiscountedPricesSynchronous() {
        long start = System.nanoTime();
        List<String> result = DiscountedShopService.findPricesSynchronous("myPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }

    @Test
    public void testFindDiscountedPricesAsynchronousOnCommonPool() {
        long start = System.nanoTime();
        List<String> result = DiscountedShopService.findPricesAsynchronousOnCommonPool("myPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }

    @Test
    public void testFindDiscountedPricesAsynchronousOnCustomExecutor() {
        long start = System.nanoTime();
        List<String> result = DiscountedShopService.findPricesAsynchronousOnCustomExecutor("myPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }
}
