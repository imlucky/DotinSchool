package com.dotin.shop;

import com.dotin.shop.domain.Shop;
import org.junit.Test;

import java.util.concurrent.Future;


public class TestServiceShop {
    Service service = new ServiceImpl();


    @Test
    public void testShop() {
        Shop shop = new Shop("BestShop");
        long start = System.nanoTime();
        Future<Double> futurePrice = shop.getPriceAsync("my favorite product");
        long invocationTime = ((System.nanoTime() - start) / 1_000_000);
        System.out.println("invocation returned after " + invocationTime + " m Secs");

        try {
            double price = futurePrice.get();
            System.out.printf("price is %.2f%n ", price);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        long retrievalTime = ((System.nanoTime() - start) / 1_000_000);
        System.out.println("price returned after " + retrievalTime + " m Secs");
    }

    @Test
    public void testFindPrices() {
        long start = System.nanoTime();
        System.out.println(service.findPrices("iphone", 2));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("done is " + duration + "m Secs");
    }

    @Test
    public void testFindPricesParallel() {
        long start = System.nanoTime();
        System.out.println(service.findPricesParallel("iphone", 8));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("done is " + duration + "m Secs");
    }

    @Test
    public void testFindPricesCompletableFuture() {
        long start = System.nanoTime();
        System.out.println(service.findPricesCompletableFuture("samsung", 2));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("done is " + duration + " m Secs");
    }

    @Test
    public void testFindPricesCompletableFutureJoin() {
        long start = System.nanoTime();
        System.out.println(service.findPricesCompletableFutureJoin("samsung", 4));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("done is " + duration + " m Secs");
    }

    @Test
    public void testFindPriceDiscount() {
        long start = System.nanoTime();
        System.out.println(service.findPricesParallel("iphone", 5));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("done is " + duration + "m Secs");
    }


}
