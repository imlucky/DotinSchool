package com.dotin.methodreference;
import com.dotin.applepredicate.Apple;
import org.junit.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.function.BiFunction;

public class TestMethodReference {




    @Test
    public void testApples() {
        AppleAttribute appleAttr = new AppleAttribute("red", 120);
        BiFunction<String,Integer,Apple> appleBI = Apple::new;
        Apple apple = appleBI.apply(appleAttr.getColor(),appleAttr.getWeight());
        assertThat(apple.getColor(),is(equalTo("red")));
    }
}







