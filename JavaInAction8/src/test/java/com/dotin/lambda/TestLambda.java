package com.dotin.lambda;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class TestLambda {

    private static List<Apple> apples;

    private Function<String, Predicate<Apple>> appleColor = color -> (apple -> color.equals(apple.getColor()));
    private Function<Integer, Predicate<Apple>> appleWeight = weight -> (apple -> weight <= apple.getWeight());


    @Before
    public void appleValues() {
        apples = Arrays.asList(new Apple("black", 120),
                new Apple("red", 170),
                new Apple("blue", 120),
                new Apple("black", 150),
                new Apple("red", 60),
                new Apple("black", 70)
        );
    }

    @Test(expected = NullPointerException.class)
    public void testNullPredicate() {
        LambdaFilter.filterLambda(apples,null);
    }

    @Test
    public void testAppleColor() {
        List<Apple> result = LambdaFilter.filterLambda(apples, appleColor.apply("red"));
        assertThat(2, equalTo(result.size()));
    }

    @Test
    public void testAppleWeight() {
        List<Apple> result = LambdaFilter.filterLambda(apples, appleWeight.apply(100));
        assertThat(4, equalTo(result.size()));
    }

    @Test
    public void testAppleColorAndWeight() {
        List<Apple> result = LambdaFilter.filterLambda(apples, appleColor.apply("red").and(appleWeight.apply(150)));
        assertThat(1, equalTo(result.size()));
    }

}
