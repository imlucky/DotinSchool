package com.dotin.applepredicate;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestApplePredicate {
    private static List<Apple> apples;


    @Before
    public void applesValues() {
        apples = Arrays.asList(new Apple("red", 150),
                new Apple("black", 150),
                new Apple("blue", 220),
                new Apple("black", 77),
                new Apple("red", 70),
                new Apple("blue", 20),
                new Apple("black", 99),
                new Apple("pink", 150),
                new Apple("blue", 180),
                new Apple("black", 30)
        );
    }

    @Test(expected = NullPointerException.class)
    public void testNullPredicate() {
        AppleFilter.filterApple(apples, null);
    }
    @Test
    public void testColorPredicate() {
        AppleColor appleColor = new AppleColor("black");
        List<Apple> resultList = AppleFilter.filterApple(apples,appleColor);
        assertThat(4, equalTo(resultList.size()));
    }

    @Test
    public void testAppleWeight() {
        AppleWeight appleWeight = new AppleWeight(100);
        List<Apple> result = AppleFilter.filterApple(apples, appleWeight);
        assertThat(5, equalTo(result.size()));
    }

    @Test
    public void testAppleColorAndWeight() {
        AppleColorAndWeight appleColorAndWeight = new AppleColorAndWeight("black", 20);
        List<Apple> result = AppleFilter.filterApple(apples, appleColorAndWeight);
        assertThat(4, equalTo(result.size()));

    }
}