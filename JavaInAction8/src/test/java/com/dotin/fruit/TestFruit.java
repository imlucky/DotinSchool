package com.dotin.fruit;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import java.util.function.BiFunction;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;

public class TestFruit {

    static Map<String, BiFunction<String, Integer, Fruit>> mapFruit;

    static {
        mapFruit = new HashMap<>();
        mapFruit.put("banana", Banana::new);
        mapFruit.put("kiwi", Kiwi::new);
    }

    @Test
    public void testBanana() {
        Banana banana = (Banana) mapFruit.get("banana").apply("Yellow", 850);
        System.out.println(banana);
        assertThat(banana, isA(Banana.class));
    }

    @Test
    public void testKiwi() {
        Kiwi kiwi = (Kiwi) mapFruit.get("kiwi").apply("Brown", 50);
        System.out.println(kiwi);
        assertThat(kiwi, isA(Kiwi.class));
    }

}

