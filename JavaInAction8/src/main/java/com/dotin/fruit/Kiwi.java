package com.dotin.fruit;

public class Kiwi extends Fruit {

    public Kiwi(String color, int weight) {
        super(color, weight);
    }

    @Override
    public String toString() {
        return "color Kiwi is: " + getColor() + "And weight is: " + getWeight() ;
    }
}
