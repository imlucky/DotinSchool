package com.dotin.fruit;

public class Banana extends Fruit {

    public Banana(String color,int weight) {
        super(color,weight);
    }

    @Override
    public String toString() {
        return "color Banana is: " + getColor() + " And weight is: " + getWeight();
    }
}
