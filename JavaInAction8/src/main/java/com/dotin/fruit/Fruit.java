package com.dotin.fruit;

public class Fruit {
    private String color;
    private int weight;

    public Fruit(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public int getWeight() {
        return weight;
    }
}
