package com.dotin.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class LambdaFilter {
    public static List<Apple> filterLambda(List<Apple> list, Predicate predicate) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : list) {
            if(predicate.test(apple)) {
                result.add(apple);
            }
        }
        return result;
    }
}
