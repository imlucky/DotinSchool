package com.dotin.applepredicate;

import java.util.ArrayList;
import java.util.List;

public class AppleFilter {
    public static List<Apple> filterApple(List<Apple> apples, ApplePredicate predicate) {
        List<Apple> resultList = new ArrayList<>();
        for (Apple apple : apples) {
            if (predicate.test(apple)) {
                resultList.add(apple);
            }
        }
        return resultList;
    }
}
