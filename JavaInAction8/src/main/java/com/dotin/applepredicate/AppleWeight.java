package com.dotin.applepredicate;

public class AppleWeight implements ApplePredicate {
    private int weight;

    public AppleWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean test(Apple apple) {
        return apple.getWeight() >= weight;
    }
}