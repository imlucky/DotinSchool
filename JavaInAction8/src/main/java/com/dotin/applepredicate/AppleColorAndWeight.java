package com.dotin.applepredicate;

public class AppleColorAndWeight implements ApplePredicate {
    private String color;
    private int weight;

    public AppleColorAndWeight(String color, int weight) {
        this.color = color;
        this.weight =weight;
    }

    public boolean test(Apple apple) {
        return color.equals(apple.getColor()) && apple.getWeight()>=weight;
    }
}