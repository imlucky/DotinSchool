package com.dotin.applepredicate;

@FunctionalInterface
public interface ApplePredicate {
    boolean test(Apple apple);
}
