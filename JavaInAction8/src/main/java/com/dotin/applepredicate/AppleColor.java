package com.dotin.applepredicate;

public class AppleColor implements ApplePredicate {
    private String color;

    public AppleColor(String color) {
        this.color = color;
    }


    public boolean test(Apple apple) {
        return color.equals(apple.getColor());
    }
}