package com.dotin.duration;

import java.util.Optional;
import java.util.Properties;

public class DurationReader {
    static Properties properties = new Properties();

    static {
        properties.setProperty("a", "5");
        properties.setProperty("b", "true");
        properties.setProperty("c", "-3");
    }

    public static int readDuration(String name) {
        String propertyValue = (String) properties.get(name);
        return Optional.ofNullable(propertyValue).flatMap(DurationReader::stringToOptionalInteger)
                .filter(i -> i >= 0).orElse(0);
    }

    private static Optional<Integer> stringToOptionalInteger(String numString) {
        try {
            return Optional.of((Integer.parseInt(numString)));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    /*public static int readDuration(Properties prop, String name) {
        String value = prop.getProperty(name);
        if(value != null) {
            try {
                int i = Integer.parseInt(value);
                if (i>0){
                    return i;
                }
            } catch (NumberFormatException e) {

            }
        }
        return 0;
    }*/


}
