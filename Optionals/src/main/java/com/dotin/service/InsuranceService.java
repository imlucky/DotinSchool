package com.dotin.service;

import com.dotin.domain.Car;
import com.dotin.domain.Insurance;
import com.dotin.domain.Person;

import java.util.Optional;

public class InsuranceService {
    public static Optional<Insurance> nullSafeFindCheapestInsurance(Optional<Person> personOp, Optional<Car> carOp) {
        return personOp.flatMap(p -> carOp.map(c -> notNullSafeFindCheapestInsurance(p, c)));
    }

    private static Insurance notNullSafeFindCheapestInsurance(Person person, Car car) {
        if (person == null) {
            throw new IllegalArgumentException("person can not be null");
        }
        if (car == null) {
            throw new IllegalArgumentException("car can not be null");
        }
        return new Insurance("iran");
    }

}
