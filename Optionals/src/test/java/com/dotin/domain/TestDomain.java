package com.dotin.domain;

import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestDomain {

    private static Person personNoCar;
    private static Person personCarInsurance;
    private static Person personCarNoInsurance;

    @BeforeClass
    public static void setupVars() {
        Insurance insurance = new Insurance("iran");
        Car carInsurance = new Car(Optional.ofNullable(insurance));
        Car carNoInsurance = new Car(Optional.empty());

        personNoCar = new Person(Optional.empty());
        personCarInsurance = new Person(Optional.ofNullable(carInsurance));
        personCarNoInsurance = new Person(Optional.ofNullable(carNoInsurance));
    }

    @Test
    public void getInsuranceNameWhenItExists() {
        Optional<String> optName = Optional.ofNullable(personCarInsurance)
                .flatMap(Person::getCar).flatMap(Car::getInsurance).map(Insurance::getName);
        assertThat(optName.get(), is(CoreMatchers.equalTo("iran")));
    }

    @Test
    public void getInsuranceNameWhenItDoesNotExist() {
        String name = Optional.ofNullable(personCarNoInsurance)
                .flatMap(Person::getCar).flatMap(Car::getInsurance).map(Insurance::getName).orElse("Unknown");
        assertThat(name, is(CoreMatchers.equalTo("Unknown")));
    }
    @Test
    public void getInsuranceNameWhenItDoesNotExistWithDefaultValue() {
        String name = Optional.ofNullable(personCarInsurance)
                .flatMap(Person::getCar).flatMap(Car::getInsurance).map(Insurance::getName).orElse("Unknown");
        assertThat(name, is(CoreMatchers.equalTo("iran")));
    }
}

