package com.dotin.chapter6;

import com.dotin.chapter4.Dish;
import com.dotin.chapter4.Menu;
import org.junit.Test;

import java.util.*;


public class TestService {
    Service service = new ServiceImpl();


    @Test
    public void testHowManyDishes() {
        long result = service.filterHowManyDishes(Menu.getDishes());
        System.out.println(result);
    }

    @Test
    public void testTotalCalories() {
        int totalCalories = service.filterTotalCalories(Menu.getDishes());
        System.out.println(totalCalories);
    }


    @Test
    public void testAverageCalories() {
        double averageCalories = service.filterAverageCalories(Menu.getDishes());
        System.out.println(averageCalories);
    }

    @Test
    public void testIntSummaryStatistics() {
        IntSummaryStatistics menuStatistics = service.filterIntSummaryStatistics(Menu.getDishes());
        System.out.println(menuStatistics);
    }

    @Test
    public void testJoining() {
        String shortMenu = service.filterShortMenu(Menu.getDishes());
        System.out.println(shortMenu);
    }

    @Test
    public void testReducing() {
        Optional<Dish> mostCalorieDish = service.filterMostCalorieDish(Menu.getDishes());
        System.out.println(mostCalorieDish);
    }

    @Test
    public void testDishesByType() {
        Map<Dish.Type, List<Dish>> dishesByType = service.filterDishesByType(Menu.getDishes());
        System.out.println(dishesByType);
    }

    @Test
    public void testDishesByCaloricLevel() {
        Map<CaloricLevel, List<Dish>> dishesByCaloricLevel = service.filterDishesByCaloricLevel(Menu.getDishes());
        System.out.println(dishesByCaloricLevel);
    }

    @Test
    public void testDishesByCaloricMultiLevel() {
        Map<Dish.Type, Map<CaloricLevel, List<Dish>>> dishesByCaloricMultiLevel = service.filterDishesByCaloricMultiLevel(Menu.getDishes());
        System.out.println(dishesByCaloricMultiLevel);
    }

    @Test
    public void testTypeCount() {
        Map<Dish.Type, Long> typeCount = service.filterTypesCount(Menu.getDishes());
        System.out.println(typeCount);
    }

    @Test
    public void testMostCaloricByType() {
        Map<Dish.Type, Optional<Dish>> mostCaloricByType = service.filterMostCaloricByType(Menu.getDishes());
        System.out.println(mostCaloricByType);
    }

    @Test
    public void testSubGroup() {
        Map<Dish.Type, Dish> subGroup = service.filterSubGroup(Menu.getDishes());
        System.out.println(subGroup);
    }

    @Test
    public void testSumSubGroup() {
        Map<Dish.Type, Integer> sumSubGroup = service.filterSumSubGroup(Menu.getDishes());
        System.out.println(sumSubGroup);
    }

    @Test
    public void testCaloricLevel() {
        Map<Dish.Type, Set<CaloricLevel>> caloricLevel = service.filterCaloricLevel(Menu.getDishes());
        System.out.println(caloricLevel);
    }

    @Test
    public void testPartition() {
        Map<Boolean, List<Dish>> partitionGroupBy = service.filterPartitionGroupBy(Menu.getDishes());
        System.out.println(partitionGroupBy);

        List<Dish> vegetarianDishes = partitionGroupBy.get(true);
        System.out.println(vegetarianDishes);
    }

    @Test
    public void testVegetarianCollect() {
        List<Dish> vegetarianDishesType = service.filterVegetarianCollect(Menu.getDishes());
        System.out.println(vegetarianDishesType);
    }

    @Test
    public void testVegetarianDishesByType() {
        Map<Boolean, Map<Dish.Type, List<Dish>>> vegetarianDishesByType = service.filterVegetarianDishesByType(Menu.getDishes());
        System.out.println(vegetarianDishesByType);
    }

    @Test
    public void testMostCaloricPartitionedByVegetarian() {
        Map<Boolean, Dish> mostCaloricPartitionedByVegetarian = service.filterMostCaloricPartitionedByVegetarian(Menu.getDishes());
        System.out.println(mostCaloricPartitionedByVegetarian);
    }
}
