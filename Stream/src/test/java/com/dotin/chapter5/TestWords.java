package com.dotin.chapter5;


import com.dotin.chapter4.Menu;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestWords {
    WordList wordList = new WordListImpl();

    @Test
    public void testWords() {
        List<Integer> lengthWords = wordList.filterLengthWords(Words.words);
        assertThat(lengthWords.size(), equalTo(7));
    }

    @Test
    public void testLengthDishes() {
        List<Integer> lengthDishes = wordList.dishNameLength(Menu.getDishes());
        assertThat(lengthDishes.size(), equalTo(9));
    }

    //Quiz 1
    @Test
    public void testSquareList() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> square = numbers.stream().map(n -> n * n).collect(toList());
        assertThat(square.size(), equalTo(5));
        square.stream().forEach(squares -> System.out.println(Arrays.toString(new Integer[]{squares})));
    }

    //Quiz 2
    @Test
    public void testPairsList() {
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> number2 = new ArrayList<>(Arrays.asList(3, 4));
        List<int[]> pairs = numbers1.stream()
                .flatMap(i -> number2.stream()
                        .map(j -> new int[]{i, j})
                )
                .collect(toList());
        assertThat(pairs.size(), equalTo(6));
        pairs.stream().forEach(pair -> System.out.println(Arrays.toString(pair)));
    }

    // Quiz 3
    @Test
    public void testSumDivide() {
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> number2 = new ArrayList<>(Arrays.asList(3, 4));
        List<int[]> pairs = numbers1.stream()
                .flatMap(i -> number2.stream()
                        .filter(j -> (i + j) % 3 == 0).map(j -> new int[]{i, j})
                )
                .collect(toList());
        assertThat(pairs.size(), equalTo(2));
        pairs.stream().forEach(pair -> System.out.println(Arrays.toString(pair)));

    }


}
