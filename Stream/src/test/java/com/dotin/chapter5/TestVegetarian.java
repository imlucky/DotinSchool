package com.dotin.chapter5;

import com.dotin.chapter4.Dish;
import com.dotin.chapter4.Menu;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestVegetarian {
    DishVegetarian dishVegetarian = new DishVegetarianImpl();


    @Test
    public void testVegetarian() {
        List<Dish> listVegetarian = dishVegetarian.filterDishesThatIsVegetarian(Menu.getDishes());
        assertThat(listVegetarian.size(), equalTo(4));
    }

    @Test
    public void testNotVegetarian() {
        List<Dish> listNotVegetarian = dishVegetarian.filterDishesThatIsNotVegetarian(Menu.getDishes());
        assertThat(listNotVegetarian.size(), equalTo(5));
    }
}
