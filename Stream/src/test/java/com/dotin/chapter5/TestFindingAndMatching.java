package com.dotin.chapter5;

import com.dotin.chapter4.Dish;
import com.dotin.chapter4.Menu;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestFindingAndMatching {
    MatcherList matcherList = new MatcherListImpl();
    FindAnyList findAnyList = new FindAnyListImpl();

    @Test
    public void testAnyMatch() {
        List<Dish> matches = matcherList.filterAnyMatcherList(Menu.getDishes());
        assertThat(matches.size(), equalTo(9));
    }

    @Test
    public void testFindAny() {
        Optional<Dish> findDishes = findAnyList.optionalDish(Menu.getDishes());
        System.out.println(findDishes);
    }
}
