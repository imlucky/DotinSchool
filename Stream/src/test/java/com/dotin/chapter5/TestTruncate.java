package com.dotin.chapter5;

import com.dotin.chapter4.Dish;
import com.dotin.chapter4.Menu;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestTruncate {
    Truncating truncating = new TruncatingImpl();

    @Test
    public void testTruncate() {
        List<Dish> dishList = truncating.filterMenuTruncate(Menu.getDishes(), 300);
        assertThat(dishList.size(), equalTo(2));
    }

    @Test
    public void testMap() {
        List<String> mapList = truncating.filterMenuMap(Menu.getDishes());
        assertThat(mapList.size(),equalTo(9));
    }

}
