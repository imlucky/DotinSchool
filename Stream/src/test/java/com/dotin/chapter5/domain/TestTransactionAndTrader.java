package com.dotin.chapter5.domain;

import org.hamcrest.MatcherAssert.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestTransactionAndTrader {
    private static List<Transaction> transactions;

    @Before
    public void setupTrader() {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");


        transactions = new ArrayList<>(Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950))
        );
    }

    @Test
    public void testQuiz1() {
        List<Transaction> transactionList = transactions.stream()
                .filter(transaction -> transaction.getYear() == 2011)
                .sorted(comparing(Transaction::getValue))
                .collect(toList());
        assertThat(transactionList.size(), equalTo(2));
    }

    @Test
    public void testQuiz2() {
        Set<String> cities = transactions.stream()
                .map(transaction -> transaction.getTrader()
                        .getCity()).collect(toSet());
        assertThat(cities.size(), equalTo(2));
    }

    @Test
    public void testQuiz3() {
        List<Trader> traders = transactions.stream()
                .map(Transaction::getTrader)
                .filter(trader -> trader.getCity().equals("Cambridge")).distinct()
                .sorted(comparing(Trader::getName)).collect(toList());
        assertThat(traders.size(), equalTo(3));
    }

    @Test
    public void testQuiz4() {
        String traderStr = transactions.stream().map(transaction -> transaction.getTrader().getName()).distinct()
                .sorted().reduce(" ", (x, y) -> x + " " + y);
        assertThat(traderStr.length(), equalTo(24));
    }

    @Test
    public void testQuiz5() {
        boolean milanBased = transactions.stream().anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
        //boolean milanBased = transactions.stream().allMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
        //boolean milanBased = transactions.stream().noneMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
        System.out.println(milanBased ? "Exist" : "notExist");
    }

    @Test
    public void testQuiz6() {
        transactions.stream().filter(t -> "Cambridge".equals(t.getTrader().getCity())).map(Transaction::getValue).forEach(System.out::println);
    }

    @Test
    public void testQuiz7() {
        //Optional<Integer> highestValue = transactions.stream().map(Transaction::getValue).reduce(Integer::max);
        Optional<Transaction> highestValue = transactions.stream().max(comparing(Transaction::getValue));
        System.out.println(highestValue);
    }

    @Test
    public void testQuiz8() {
        //Optional<Transaction> smallestTransaction = transactions.stream().reduce((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2);
        Optional<Transaction> smallestTransaction = transactions.stream().min(comparing(Transaction::getValue));
        System.out.println(smallestTransaction);
    }

    @Test
    public void testStream() {
        Stream<String> stream = Stream.of("Java8", "Lambda", "In", "Action");
        stream.map(String::toUpperCase).forEach(System.out::println);
    }

    @Test
    public void testEmptyStream() {
        //Stream<String> emptyStream = Stream.empty();
        Stream.iterate(0, n -> n + 2).limit(7).forEach(System.out::println);
    }

    @Test
    public void testArrayStream() {
        int[] numbers = {5, 6, 3, 8, 4, 9, 7, 1, 2};
        int sum = Arrays.stream(numbers).sum();
        System.out.println(sum);
    }

    @Test
    public void testFileNIO() {
        long uniqueWord = 0;
        try (Stream<String> lines = Files.lines(Paths.get("D:\\fileLine\\data.txt"), Charset.defaultCharset())) {
            uniqueWord = lines.flatMap(line -> Arrays.stream(line.split(""))).distinct().count();
        } catch (IOException e) {
            System.out.println("File not found");
        }
        System.out.println(uniqueWord);
    }

    @Test
    public void testFibonacciSeriesIterator() {
        //Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1], t[0] + t[1]}).limit(20).forEach(t -> System.out.println("(" + t[0] + "," + t[1] + ")"));
        Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1], t[0] + t[1]}).limit(20).map(t -> t[0]).forEach(System.out::println);
    }

    @Test
    public void testGenerateIterator() {
        Stream.generate(Math::random).limit(5).forEach(System.out::println);
    }

    @Test
    public void testIntSupplierFibonacci() {
        IntSupplier fib = new IntSupplier() {
            private int previous = 0;
            private int current = 1;

            @Override
            public int getAsInt() {
                int oldPrevious = this.previous;
                int nextValue = this.previous + this.current;
                this.previous = this.current;
                this.current = nextValue;
                return oldPrevious;
            }
        };
        IntStream.generate(fib).limit(10).forEach(System.out::println);
    }


}
