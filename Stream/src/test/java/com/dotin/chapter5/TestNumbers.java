package com.dotin.chapter5;

import org.junit.Test;

import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestNumbers {
    Numbers numbers = new NumbersImpl();


    @Test
    public void testNumber() {
        List<Integer> integerList = numbers.filterUniqueNumbers(NumbersList.numbers, 3);
        assertThat(integerList.size(), equalTo(13));
    }
}
