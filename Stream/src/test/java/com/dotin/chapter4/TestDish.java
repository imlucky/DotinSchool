package com.dotin.chapter4;




import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import java.util.List;

public class TestDish {
    DishService dishService = new DishServiceImpl();

    @Test
    public void testDish() {
        List<String> list = dishService.filterDishesHavingCaloriesMoreThan(Menu.getDishes(), 300);
        assertThat(list.size(), equalTo(3));
    }
}
