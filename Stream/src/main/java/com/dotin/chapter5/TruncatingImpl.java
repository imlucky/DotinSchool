package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class TruncatingImpl implements Truncating {
    @Override
    public List<Dish> filterMenuTruncate(List<Dish> dishList, int calories) {
        //return dishList.stream().filter(d -> d.getCalories()>300).skip(2).limit(5).collect(toList());
        return dishList.stream().filter(d -> d.getType() == Dish.Type.MEAT).limit(2).collect(toList());
    }

    @Override
    public List<String> filterMenuMap(List<Dish> mapList) {
        return mapList.stream().map(Dish::getName).collect(toList());
    }
}
