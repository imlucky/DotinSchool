package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

public interface Truncating {
    List<Dish> filterMenuTruncate(List<Dish> dishList, int calories);
    List<String> filterMenuMap(List<Dish> mapList);
}
