package com.dotin.chapter5;

import java.util.List;

public interface Numbers {
    List<Integer> filterUniqueNumbers(List<Integer> numbersList, Integer numUnique);
}
