package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.toList;

public class DishVegetarianImpl implements DishVegetarian {
    @Override
    public List<Dish> filterDishesThatIsVegetarian(List<Dish> dishList) {
        return dishList.stream().filter(Dish::isVegetarian).collect(toList());
    }

    @Override
    public List<Dish> filterDishesThatIsNotVegetarian(List<Dish> dishList) {
        Predicate<Dish> isNotVegetarian = Dish::isVegetarian;
        return dishList.stream().filter(isNotVegetarian.negate()).collect(toList());
    }
}
