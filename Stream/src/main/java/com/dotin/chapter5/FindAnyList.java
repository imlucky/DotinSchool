package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;
import java.util.Optional;

public interface FindAnyList {
    Optional<Dish> optionalDish(List<Dish> dishList);
}
