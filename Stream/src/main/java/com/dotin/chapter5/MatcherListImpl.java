package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

public class MatcherListImpl implements MatcherList {
    @Override
    public List<Dish> filterAnyMatcherList(List<Dish> dishList) {
        if (dishList.stream().anyMatch(Dish::isVegetarian)){
            System.out.println("The menu is (somewhat) vegetarian friendly!!");
        };
        return dishList;
    }
}
