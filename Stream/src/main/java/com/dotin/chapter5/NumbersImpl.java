package com.dotin.chapter5;

import java.util.List;

public class NumbersImpl implements Numbers {
    @Override
    public List<Integer> filterUniqueNumbers(List<Integer> numbersList, Integer numUnique) {
        numbersList.stream().filter(i -> i % 2 == 0).distinct().forEach(System.out::println);
        return numbersList;
    }
}
