package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

public interface DishVegetarian {
    List<Dish> filterDishesThatIsVegetarian(List<Dish> dishList);
    List<Dish> filterDishesThatIsNotVegetarian(List<Dish> dishList);
}
