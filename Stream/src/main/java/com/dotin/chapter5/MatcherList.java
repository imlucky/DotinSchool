package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

public interface MatcherList {
    List<Dish> filterAnyMatcherList(List<Dish> dishList);
}
