package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;
import java.util.Optional;

public class FindAnyListImpl implements FindAnyList {
    @Override
    public Optional<Dish> optionalDish(List<Dish> dishList) {
        return dishList.stream().filter(Dish::isVegetarian).findAny();
    }
}
