package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

public interface WordList {
    List<Integer> filterLengthWords(List<String> words);
    List<Integer> dishNameLength(List<Dish> dishes);

}
