package com.dotin.chapter5;

import com.dotin.chapter4.Dish;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class WordListImpl implements WordList {
    @Override
    public List<Integer> filterLengthWords(List<String> words) {
        return words.stream().map(String::length).collect(toList());
    }

    @Override
    public List<Integer> dishNameLength(List<Dish> dishes) {
        return dishes.stream().map(Dish::getName).map(String::length).collect(toList());
    }
}
