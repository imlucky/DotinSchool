package com.dotin.chapter6;

import com.dotin.chapter4.Dish;
import com.dotin.chapter4.Menu;

import java.util.*;

public interface Service {
    long filterHowManyDishes(List<Dish> dishList);

    int filterTotalCalories(List<Dish> dishList);

    double filterAverageCalories(List<Dish> dishList);

    IntSummaryStatistics filterIntSummaryStatistics(List<Dish> dishList);

    String filterShortMenu(List<Dish> dishList);

    Optional<Dish> filterMostCalorieDish(List<Dish> dishList);

    Map<Dish.Type, List<Dish>> filterDishesByType(List<Dish> dishList);

    Map<CaloricLevel, List<Dish>> filterDishesByCaloricLevel(List<Dish> dishList);

    Map<Dish.Type, Map<CaloricLevel, List<Dish>>> filterDishesByCaloricMultiLevel(List<Dish> dishList);

    Map<Dish.Type, Long> filterTypesCount(List<Dish> dishList);

    Map<Dish.Type, Optional<Dish>> filterMostCaloricByType(List<Dish> dishList);

    Map<Dish.Type, Dish> filterSubGroup(List<Dish> dishList);

    Map<Dish.Type, Integer> filterSumSubGroup(List<Dish> dishList);

    Map<Dish.Type, Set<CaloricLevel>> filterCaloricLevel(List<Dish> dishList);

    Map<Boolean, List<Dish>> filterPartitionGroupBy(List<Dish> dishList);

    List<Dish> filterVegetarianCollect(List<Dish> dishList);

    Map<Boolean, Map<Dish.Type, List<Dish>>> filterVegetarianDishesByType(List<Dish> dishList);

    Map<Boolean, Dish> filterMostCaloricPartitionedByVegetarian(List<Dish> dishList);
}
