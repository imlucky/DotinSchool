package com.dotin.chapter6;

import com.dotin.chapter4.Dish;
import com.sun.tracing.dtrace.ArgsAttributes;

import javax.xml.bind.annotation.XmlAnyAttribute;
import java.util.*;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;

public class ServiceImpl implements Service {
    @Override
    public long filterHowManyDishes(List<Dish> dishList) {
        return dishList.stream().count();
    }

    @Override
    public int filterTotalCalories(List<Dish> dishList) {
        return dishList.stream().collect(summingInt(Dish::getCalories));
    }

    @Override
    public double filterAverageCalories(List<Dish> dishList) {
        return dishList.stream().collect(averagingInt(Dish::getCalories));
    }

    @Override
    public IntSummaryStatistics filterIntSummaryStatistics(List<Dish> dishList) {
        return dishList.stream().collect(summarizingInt(Dish::getCalories));
    }

    @Override
    public String filterShortMenu(List<Dish> dishList) {
        return dishList.stream().map(Dish::getName).collect(joining(","));
        //return dishList.stream().collect(joining());
    }

    @Override
    public Optional<Dish> filterMostCalorieDish(List<Dish> dishList) {
        return dishList.stream().collect(reducing((d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
    }

    @Override
    public Map<Dish.Type, List<Dish>> filterDishesByType(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType));
    }

    @Override
    public Map<CaloricLevel, List<Dish>> filterDishesByCaloricLevel(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                }
        ));
    }

    @Override
    public Map<Dish.Type, Map<CaloricLevel, List<Dish>>> filterDishesByCaloricMultiLevel(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                }
        )));
    }

    @Override
    public Map<Dish.Type, Long> filterTypesCount(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, counting()));
    }

    @Override
    public Map<Dish.Type, Optional<Dish>> filterMostCaloricByType(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, maxBy(comparingInt(Dish::getCalories))));
    }

    @Override
    public Map<Dish.Type, Dish> filterSubGroup(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));
    }

    @Override
    public Map<Dish.Type, Integer> filterSumSubGroup(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, summingInt(Dish::getCalories)));
    }

    @Override
    public Map<Dish.Type, Set<CaloricLevel>> filterCaloricLevel(List<Dish> dishList) {
        return dishList.stream().collect(groupingBy(Dish::getType, mapping(
                dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() < 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;

                //}, toSet())));
                }, toCollection(HashSet::new))));
    }

    @Override
    public Map<Boolean, List<Dish>> filterPartitionGroupBy(List<Dish> dishList) {
        return dishList.stream().collect(partitioningBy(Dish::isVegetarian));
    }

    public List<Dish> filterVegetarianCollect(List<Dish> dish) {
        return dish.stream().filter(Dish::isVegetarian).collect(toList());
    }

    @Override
    public Map<Boolean, Map<Dish.Type, List<Dish>>> filterVegetarianDishesByType(List<Dish> dishList) {
        return dishList.stream().collect(partitioningBy(Dish::isVegetarian, groupingBy(Dish::getType)));
    }

    @Override
    public Map<Boolean, Dish> filterMostCaloricPartitionedByVegetarian(List<Dish> dishList) {
        return dishList.stream().collect(partitioningBy(Dish::isVegetarian, collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));
    }
}
