package com.dotin.chapter4;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class DishServiceImpl implements DishService {
    @Override
    public List<String> filterDishesHavingCaloriesMoreThan(List<Dish> dishes, int calories) {
        return dishes.stream().filter(d -> d.getCalories() > calories).map(Dish::getName).limit(3).collect(toList());
    }
}
