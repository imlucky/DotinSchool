package com.dotin.chapter4;

import java.util.List;

public interface DishService {
    List<String> filterDishesHavingCaloriesMoreThan(List<Dish> dishes, int calories);
}
