package javaGeek;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class RunnerTablePerClass {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TablePerClassInheritance");

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        Truck truck = new Truck();
        truck.setManufacturer("mercedes");
        truck.setLoadCapacity(100);
        truck.setNoOfContainers(2);
        em.persist(truck);

        Car car = new Car();
        car.setManufacturer("Audi");
        car.setNoOfDoors(2);
        car.setNoOfPassengers(20);
        em.persist(car);

        Bike bike = new Bike();
        bike.setManufacturer("hummer");
        bike.setSaddleHeight(77);
        bike.setNoOfPassengers(4);
        em.persist(bike);

        transaction.commit();
    }
}
